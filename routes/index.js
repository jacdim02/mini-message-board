const express = require("express");
const router = express.Router();

const messages = [
  {
    text: "Hi there!",
    user: "Amando",
    added: new Date().toLocaleString(),
  },
  {
    text: "Hello World!",
    user: "Charles",
    added: new Date().toLocaleString(),
  },
];

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Mini Messageboard", messages: messages });
});

router.get("/new", function (req, res, next) {
  res.render("new", { title: "Add new message" });
});

router.post("/new", function (req, res, next) {
  const { user, text } = req.body;
  messages.push({ user, text, added: new Date().toLocaleString() });
  res.redirect("/");
});

module.exports = router;
